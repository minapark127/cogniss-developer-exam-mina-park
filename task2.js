const jsonfile = require('jsonfile');
const randomstring = require('randomstring');
const got = require('got');

const inputURL = 'http://www.nactem.ac.uk/software/acromine/dictionary.py?sf=';
const outputFile = 'output3.json';

var output = {};

console.log('getting 10 acronyms');
output.definitions = [];

console.log('creating looping function');
const getAcronym = async function () {
  var acronym = randomstring.generate(3).toUpperCase();

  await got(inputURL + acronym)
    .then((response) => {
      console.log('got data for acronym', acronym);
      console.log('add returned data to definitions array');
      output.definitions.push(response.body);
      console.log(`--- pushed ${output.definitions.length} data`);
    })
    .catch((err) => {
      console.log(err);
    });

  if (output.definitions.length < 10) {
    console.log('calling looping function again');
    await getAcronym();
  }
};

const pushTenAcronym = getAcronym();

console.log('calling looping function');
pushTenAcronym
  .then(() => {
    console.log('saving output file formatted with 2 space indenting');
    jsonfile.writeFile(outputFile, output, { spaces: 2 }, function (err) {
      if (err) {
        console.log(err);
        return;
      }
      console.log('All done!');
    });
  })
  .catch((error) => console.log(error));
