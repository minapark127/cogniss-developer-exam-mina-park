const jsonfile = require('jsonfile');
const randomstring = require('randomstring');

const inputFile = 'input2.json';
const outputFile = 'output2.json';

var output = {};

jsonfile.readFile(inputFile, (error, body) => {
  if (error) {
    console.log(error);
    return;
  }

  const names = body.names;

  let reversedNames = [];
  names.forEach((name) => reversedNames.push([...name].reverse().join('')));
  console.log('reversed names', reversedNames);

  output.emails = [];

  reversedNames.forEach((name) =>
    output.emails.push(`${name}${randomstring.generate(5)}@gmail.com`)
  );
  console.log(
    'generated fake email addresses and pushed to output',
    output.emails
  );

  console.log('saving output file formatted with 2 space indenting');
  jsonfile.writeFile(outputFile, output, { spaces: 2 }, (error) => {
    if (error) {
      console.log(error);
      return;
    }

    console.log('All done!');
  });
});
